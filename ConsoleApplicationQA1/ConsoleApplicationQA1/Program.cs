﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Threading;

namespace ConsoleApplicationQA1
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Url = "http://www.kinopoisk.ru";
            IWebElement searchField = driver.FindElement(By.XPath("//form/input[@name='kp_query']"));
            searchField.SendKeys("Sasha");
            IWebElement searchButton = driver.FindElement(By.XPath("//input[@value='искать!']"));
            searchButton.Click();
            Thread.Sleep(3000);
            driver.Close();
        }
    }
}
